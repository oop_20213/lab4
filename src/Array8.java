import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();

        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");

        }
        System.out.println();

        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];

        }
        System.out.println("sum = " + sum);

        double avg = ((double) sum) / arr.length;
        System.out.println("avg = " + avg);

        int minIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < arr[minIndex]) {
                minIndex = i;
            }
        }
        System.out.println("min = " + arr[minIndex] + " index = " + minIndex);

        int maxIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            if(arr[i]>arr[maxIndex]){
                maxIndex = i;
            }
            
        }
        System.out.println("max = " + arr[maxIndex] + " index = " + maxIndex);
    }   
        
}
